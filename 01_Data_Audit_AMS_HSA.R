
## "Campaign Headline Search Ads 15 june 13 September.xlsx"

CampHeadSearchAds15june13Sept <- read_excel("data/Campaign Headline Search Ads 15 june 13 September.xlsx")

names(CampHeadSearchAds15june13Sept) <- c("Date",
                                          "Currency",
                                          "CampaignName",
                                          "Impression",
                                          "Clicks",
                                          "ClickThroughRate",
                                          "CostPerClick",
                                          "Expenditure",
                                          "TotalAdvertisingCosts",
                                          "TotalReturnOnAdSpend",
                                          "14DaysTotalSales",
                                          "14DaysOrdersTotal",
                                          "14DaysTotalUnits",
                                          "14DayConversionRate",
                                          "14DayBrandTotDetailPageView")

da_r(CampHeadSearchAds15june13Sept)

## "Campaign Placement Headline Search Ads 15 june 13 September.xlsx"

CampPlaceHeadSearchAds15june13Sept <- read_excel("data/Campaign Placement Headline Search Ads 15 june 13 September.xlsx")

names(CampPlaceHeadSearchAds15june13Sept) <- c("Date",
                                               "Currency",
                                               "CampaignName",
                                               "PlacementType",
                                               "Impression",
                                               "Clicks",
                                               "ClickThroughRate",
                                               "CostPerClick",
                                               "Expenditure",
                                               "TotalAdvertisingCosts",
                                               "TotalReturnOnAdSpend",
                                               "14DaysTotalSales",
                                               "14DaysOrdersTotal",
                                               "14DaysTotalUnits",
                                               "14DayConversionRate",
                                               "14DayBrandTotDetailPageView")

da_r(CampPlaceHeadSearchAds15june13Sept)

## "Keyword Placement Headline Search Ads 15 june 13 September.xlsx"

KeywordPlaceHeadSearchAds15june13Sept <- read_excel("data/Keyword Placement Headline Search Ads 15 june 13 September.xlsx")


names(KeywordPlaceHeadSearchAds15june13Sept) <- c("Date",
                                                  "Currency",
                                                  "CampaignName",
                                                  "Keyword",
                                                  "Match Type",
                                                  "PlacementType",
                                                  "Impression",
                                                  "Clicks",
                                                  "ClickThroughRate",
                                                  "CostPerClick",
                                                  "Expenditure",
                                                  "TotalAdvertisingCosts",
                                                  "TotalReturnOnAdSpend",
                                                  "14DaysTotalSales",
                                                  "14DaysOrdersTotal",
                                                  "14DaysTotalUnits",
                                                  "14DayConversionRate",
                                                  "14DayBrandTotDetailPageView")

da_r(KeywordPlaceHeadSearchAds15june13Sept)

## "Keywords Headline Search Ads 15 june 13 September.xlsx"

KeywordsHeadSearchAds15june13Sept <- read_excel("data/Keywords Headline Search Ads 15 june 13 September.xlsx")

names(KeywordsHeadSearchAds15june13Sept) <- c("Date",
                                              "Currency",
                                              "CampaignName",
                                              "Keyword",
                                              "MatchType",
                                              "Impression",
                                              "Clicks",
                                              "ClickThroughRate",
                                              "CostPerClick",
                                              "Expenditure",
                                              "TotalAdvertisingCosts",
                                              "TotalReturnOnAdSpend",
                                              "14DaysTotalSales",
                                              "14DaysOrdersTotal",
                                              "14DaysTotalUnits",
                                              "14DayConversionRate",
                                              "14DayBrandTotDetailPageView")

da_r(KeywordsHeadSearchAds15june13Sept)
